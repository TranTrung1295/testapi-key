<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Esc\Shopify\API;

class shopifyController extends Controller
{
    //
    public function index()
    {
        $shopifyApi = new API();
        $products = $shopifyApi->call('GET', 'admin/products.json');
        return view('index', (array) $products);
    }
}
